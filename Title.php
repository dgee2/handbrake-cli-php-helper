<?php

include_once 'SubtitleTrack.php';
include_once 'AudioTrack.php';
include_once 'HandbrakeCLI.php';
include_once 'Storage.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Title
 *
 * @author Daniel Gee <dan@dan-gee.co.uk>
 */
class Title {

	/**
	 *
	 * @var AudioTrack 
	 */
	public $audioTracks = array();

	/**
	 *
	 * @var SubtitleTrack 
	 */
	public $subtitleTracks = array();
	public $title;
	public $stream;

	public function __construct($title, $stream) {
		$this->title = $title;
		$this->stream = $stream;
	}

	/**
	 * 
	 * @param type $input
	 * @return Title[] Description
	 */
	public static function parseInputString($input) {
		$input = explode("\n", strstr($input, "+ title 1:"));
		$input = array_map('trim', $input);
		$input = array_filter($input);
		array_pop($input);
		$input = array_values($input);
		$titles = array_keys(preg_grep("/\+\ title\ [0-9]+:/", $input));
		$titles[] = count($input);
		$retTitles = array();
		for ($i = 0; $i < count($titles) - 1; $i++) {
			$retTitles[] = self::parseSingleTitle(array_slice($input, $titles[$i], $titles[$i + 1] - $titles[$i]));
		}
		return $retTitles;
	}

	public static function parseSingleTitle($titleArray) {
		$title = preg_match("/[0-9]+/", $titleArray[0]);
		$stream = str_replace("+ stream: ", "", $titleArray[1]);
		$returnTitle = new Title($title, $stream);
		$chapterId = array_search("+ chapters:", $titleArray);
		$audioTrackId = array_search("+ audio tracks:", $titleArray);
		$subtitleId = array_search("+ subtitle tracks:", $titleArray);

		foreach (array_slice($titleArray, $audioTrackId + 1, $subtitleId - $audioTrackId - 1) as $value) {
			$returnTitle->audioTracks[] = AudioTrack::parseString($value);
		}
		foreach (array_slice($titleArray, $subtitleId + 1, count($titleArray) - $subtitleId - 1) as $value) {
			$returnTitle->subtitleTracks[] = SubtitleTrack::parseString($value);
		}
		return $returnTitle;
	}

	public function encode($output) {
		if (HandbrakeCLI::encodeTitle($this, $output)) {
			Storage::instance()->addVideo($this->stream);
		}
	}

	public function encoded() {
		return Storage::instance()->videoExists($this->stream);
	}

}
