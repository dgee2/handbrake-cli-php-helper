<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SubtitleTrack
 *
 * @author Daniel Gee <dan@dan-gee.co.uk>
 */
class SubtitleTrack {

	public $id;
	public $language;

	public function __construct($id, $language) {
		$this->id = $id;
		$this->language = $language;
	}
	
	public static function parseString($string) {
		preg_match("/[0-9]+/", $string, $id);
		$id = $id[0];
		$string = preg_replace("/\+ $id, /", "", $string);
		preg_match("/[a-zA-Z]+/", $string, $language);
		$language = $language[0];
		return new SubtitleTrack($id, $language);
	}

}
