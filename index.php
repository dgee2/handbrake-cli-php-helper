#!php
<?php
/**
 * Script detects audio and subtitle streams and reencodes video
 * TODO: add loads of cool stuff
 */
include_once 'Title.php';

extract(getopt('', array('in:', 'out:')));

if (!isset($in))
	die("in must be passed");

if (!isset($out)) {
	$out = dirname($in) . "\\out\\" . basename($in);
	if (!is_dir(dirname($out))) {
		mkdir(dirname($out));
	}
}

$title = HandbrakeCLI::getTitle($in);
if (!$title->encoded()) {
	$title->encode($out);
}