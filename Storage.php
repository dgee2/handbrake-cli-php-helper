<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Storage
 *
 * @author Daniel Gee <dan@dan-gee.co.uk>
 */
class Storage {

	protected $db;
	protected static $self;

	protected function __construct() {
		Storage::$self = $this;
		$this->db = new SQLite3(__DIR__ . "/Handbrake.db");

		$result = $this->db->query("SELECT name FROM sqlite_master WHERE type='table' AND name='Videos';");
		if ($result->fetchArray() === FALSE) {
			$this->db->exec("CREATE TABLE Videos (name TEXT)");
		}
	}

	public static function instance() {
		if (isset(self::$self)) {
			return self::$self;
		} else {
			return new Storage();
		}
	}

	public function videoExists($file) {
		$result = $this->db->query("SELECT name FROM videos WHERE name='" . $this->db->escapeString($file) . "'");
		return $result->fetchArray() !== FALSE;
	}

	public function addVideo($file) {
		$this->db->exec("INSERT INTO videos (name) VALUES ('" . $this->db->escapeString($file) . "')");
	}

}
