#!php
<?php
/**
 * Parse a whole directory
 */
include_once 'Title.php';

extract(getopt('', array('in:', 'out:')));

if (!isset($in) || !isset($out))
	die("in and out directories must be passed");
$in = preg_replace('/([^\/]+)$/', '$1/', $in);
$out = preg_replace('/([^\/]+)$/', '$1/', $out);

foreach (glob("*.*") as $filename) {
	$title = HandbrakeCLI::getTitle($in . $filename);
	if (!$title->encoded()) {
		if (count($title->subtitleTracks) > 1) {
			$title->subtitleTracks = array($title->subtitleTracks[0]);
		}
		$title->encode($out . $filename);
	}
}