<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HandbrakeCLI
 *
 * @author Daniel Gee <dan@dan-gee.co.uk>
 */
class HandbrakeCLI {

	/**
	 * 
	 * @param string $file
	 * @return Title[]
	 */
	public static function getTitles($file) {
		$analysis = shell_exec("HandbrakeCLI -i \"$file\" -t0 --min-duration=10 2>&1");
		return Title::parseInputString($analysis);
	}

	/**
	 * 
	 * @param string $file
	 * @return Title
	 */
	public static function getTitle($file) {
		return self::getTitles($file)[0];
	}

	/**
	 * 
	 * @param Title $title
	 * @param string $output
	 */
	public static function encodeTitle($title, $output) {
		$command = "HandBrakeCLI -i \"$title->stream\" -o \"$output\" --preset=\"Normal\"";
		$audioIds = array();
		$subtitleIds = array();

		foreach ($title->audioTracks as $audio) {
			$audioIds[] = $audio->id;
		}
		foreach ($title->subtitleTracks as $subtitle) {
			$subtitleIds[] = $subtitle->id;
		}

		if (count($audioIds) > 0) {
			$command .= " -a " . implode(",", $audioIds);
			$command .= " -E " . implode(",", array_fill(0, count($audioIds), "copy"));
		}
		if (count($subtitleIds) > 0) {
			$command .= " --subtitle " . implode(",", $subtitleIds);
		}
		passthru($command, $ret);
		return $ret == 0;
	}

}
