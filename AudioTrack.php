<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AudioTrack
 *
 * @author Daniel Gee <dan@dan-gee.co.uk>
 */
class AudioTrack {
	public $id;
	public $language;
	public $type;
	public $channels;
	
	public function __construct($id, $language = null, $type = null, $channels = null) {
		$this->id = $id;
		$this->language = $language;
		$this->type = $type;
		$this->channels = $channels;
	}
	
	public static function parseString($string) {
		preg_match("/[0-9]+/", $string, $id);
		$id = $id[0];
		return new AudioTrack($id);
	}
}
